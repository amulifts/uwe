// File name: shapes.cpp
// Author: Aman Khadka
// Assignment: Worksheet 4 - Task 2
// Date: 01/10/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: This program demonstrates the use of a class hierarchy, operator overloading and exception handling to modify and display circle and ellipse objects

#include <iostream>
using namespace std;

class Circle
{
    float rad; // The radius of the circle.
public:
    Circle(int r) : rad(r) {}            // Constructor for the Circle class.
    float getRadius() { return rad; }    // Get the radius of the circle.
    void setRadius(float r) { rad = r; } // Set the radius of the circle.

    // declaration of - operator for circle class
    Circle &operator-(int);

    // declaration of show function
    void show();
};

// define the - operator for circle class. when circle object is subtracted by int, radius is subtracted by that int. if radius is negative, throw 10.
Circle &Circle::operator-(int n)
{
    float rad = getRadius() - n; // subtract n from radius
    if (rad < 0)
    {
        throw 10; // throw 10 if radius is negative
    }
    setRadius(rad); // set radius to new value
    return *this;   // return circle object
}

// display radius of circle
void Circle::show()
{
    cout << "Radius of circle: " << getRadius() << endl;
}

// define ellipse class, which is derived from circle class
class Ellipse : public Circle
{
    float axis; // The semi-major axis of the ellipse.
public:
    Ellipse(int r, int s) : Circle(r), axis(s) {} // Constructor for the Ellipse class. rad is the radius, s is the semi-major axis.
    float getSemiMajorAxis() { return axis; }     // get semi-major axis of ellipse
    void setSemiMajorAxis(float s) { axis = s; }  // set semi-major axis of ellipse

    // declaration of - operator for ellipse class
    Ellipse &operator-(int n);

    // display radius and semi-major axis of ellipse
    void show();
};

// define the - operator for ellipse class. when ellipse object is subtracted by int, radius is subtracted by that int. if radius is negative, throw 10. semi-major axis is also subtracted by that int. if semi-major axis is negative, throw 20.
Ellipse &Ellipse::operator-(int n)
{
    axis = axis - n; // subtract n from semi-major axis
    if (axis < 0)
    {
        throw 20; // throw 20 if semi-major axis is negative
    }
    setSemiMajorAxis(axis); // set semi-major axis to new value

    float rad = getRadius() - n; // subtract n from radius
    if (rad < 0)
    {
        throw 10; // throw 10 if radius is negative
    }
    setRadius(rad); // set radius to new value

    return *this; // return ellipse object
}

// display radius and semi-major axis of ellipse
void Ellipse::show()
{
    cout << "Radius of ellipse: " << getRadius() << " Semi-major axis of ellipse: " << getSemiMajorAxis() << endl;
}

// can pass in either circle or ellipse object, and subtract int from it.
template <typename T>
void f(T &c, int n)
{
    try
    {
        T temp = c - n; // subtract n from circle or ellipse object
        temp.show();    // display radius or radius and semi-major axis
    }
    catch (int e)
    {
        // value 10 is thrown when radius of circle or ellipse is negative, value 20 is thrown when semi-major axis of ellipse is negative.
        if (e == 10)
        {
            cout << "Error: radius is negative" << endl;
        }
        else if (e == 20)
        {
            cout << "Error: semi-major axis is negative" << endl;
        }
    }
}

int main()
{
    cout << "----------------------------------------" << endl;
    cout << "Before all the operations:" << endl;
    cout << "----------------------------------------" << endl;
    Circle cir(5); // Create a circle with radius 5.
    cout << "Radius of cir: " << cir.getRadius() << endl;
    Ellipse ell(10, 6); // Create an ellipse with radius 10 and semi-major axis 6.
    cout << "Radius of ell: " << ell.getRadius() << " Semi-major axis of ell: " << ell.getSemiMajorAxis() << endl;
    Circle &r1 = cir;
    Ellipse &r2 = ell;
    cout << "----------------------------------------" << endl;
    cout << "After all the operations:" << endl;
    cout << "----------------------------------------" << endl;
    try
    {
        f(r1, 3);  // Subtract 3 from the radius of the circle.
        f(r2, 1);  // Subtract 1 from the radius and semi-major axis of the ellipse.
        f(r2, 10); // Subtract 10 from the radius and semi-major axis of the ellipse.
    }
    catch (int e)
    {
        // value 10 is thrown when radius of circle or ellipse is negative, value 20 is thrown when semi-major axis of ellipse is negative.
        if (e == 10)
        {
            cout << "Error: radius is negative" << endl;
        }
        else if (e == 20)
        {
            cout << "Error: semi-major axis is negative" << endl;
        }
    }
    return 0;
}