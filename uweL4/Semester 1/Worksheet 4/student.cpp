// File name: student.cpp
// Author: Aman Khadka
// Assignment: Worksheet 4 - Task 1
// Date: 01/10/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: This program demonstrates the use of operator overloading and constructors and destructors in a student class, and compare the student by number of passed courses by overloading greater than operator.


#include <iostream>
#include <string>

using namespace std;

class Student
{
private:
    int code;      // Student's identifier
    int courses;   // Number of courses
    float *grades; // pointer to a float ( stores memory address of a float value )
    string name;
    int age;

public:
    Student(int code = 0, int courses = 0, float *grades = 0, string name = "", int age = 0)
        : code(code), courses(courses), name(name), age(age)
    {
        // it creates a new array of courses elements to store the grades
        this->grades = new float[courses];
    }

    // to deallocate the memory allocated for the array of grades. To ensure memory is properly released when Student objects are destroyed.
    ~Student() { delete[] grades; }

    // Copy constructor (deleted)
    Student(Student &) = delete;

    // Assignment operator (deleted)
    Student &operator=(Student &) = delete;

    // return the code of the student
    int getCode() { return code; }

    // Overloaded input operator
    friend istream &operator>>(istream &is, Student &s);

    // Overloaded output operator
    friend ostream &operator<<(ostream &os, Student &s);

    // Overloaded comparison operator
    friend int operator>(Student &s1, Student &s2);
};

istream &operator>>(istream &is, Student &s)
{
    // Read code and number of courses
    cout << "Enter name of student: ";
    is >> s.name;
    cout << "Enter code of student: ";
    is >> s.code;
    cout << "Enter age of student: ";
    is >> s.age;
    cout << "Enter number of courses: ";
    is >> s.courses;

    // deallocate the memory allocated for the array of grades
    delete[] s.grades;

    // Allocate memory for the new grades array
    s.grades = new float[s.courses];

    // Read the grades
    for (int i = 0; i < s.courses; ++i)
    {
        cout << "Enter grade of course " << i + 1 << ": ";
        is >> s.grades[i];
    }
    return is;
}

ostream &operator<<(ostream &os, Student &s)
{
    // Print code and number of courses
    os << "Name of student: " << s.name << endl;
    os << "Age of student: " << s.age << endl;
    os << "Code of student: " << s.code << endl;
    os << "Number of courses: " << s.courses << endl;

    // Print the grades
    for (int i = 0; i < s.courses; ++i)
    {
        os << "Grade of course " << i + 1 << ": " << s.grades[i] << endl;
    }
    return os;
}

int operator>(Student &s1, Student &s2)
{
    // Compare the number of passed courses
    int passedCourse1 = 0;
    int passedCourse2 = 0;

    for (int i = 0; i < s1.courses; i++)
    {
        if (s1.grades[i] >= 5)
            passedCourse1++;
    }

    for (int i = 0; i < s2.courses; i++)
    {
        if (s2.grades[i] >= 5)
            passedCourse2++;
    }

    if (passedCourse1 > passedCourse2)
        return 1;
    else if (passedCourse1 < passedCourse2)
        return 2;
    else if (passedCourse1 == passedCourse2)
        return 3; // if the number of passed courses is the same, return 0
    else
        return 0;
}

int main()
{
    Student s1, s2;
    cin >> s1;
    cout << endl;
    cin >> s2;

    cout << endl;

    cout << s1;
    cout << endl;
    cout << s2;
    cout << endl;

    int i = s1 > s2;

    if (i == 1)
        cout << "Code of student who has succeeded in most courses: " << s1.getCode() << endl;
    else if (i == 2)
        cout << "Code of student who has succeeded in most courses: " << s2.getCode() << endl;
    else if (i == 3)
        cout << s1.getCode() << " and " << s2.getCode() << " have succeeded in the same number of courses" << endl;

    return 0;
}
