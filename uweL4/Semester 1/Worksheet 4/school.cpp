// File name: school.cpp
// Author: Aman Khadka
// Assignment: Worksheet 4 - Task 3
// Date: 01/10/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: This program demonstrates the use of virtual inheritance in C++. It has several classes such as School, Programming, Network, and Student that are derived from each other. It also shows the use of virtual functions in overriding the behavior of inherited functions in derived classes and creating reference to base class object from derived class object.

#include <iostream>
#include <string>
using namespace std;

class School
{
private:
    string name;

public:
    // const indicate the value of n will not be modified by the constructor.
    // & indicate the value of n will be passed by reference to a string object rather than a copy of the string object
    School(const string &n) : name(n) {}

    // this default constructor can be used to create an object of school class without providing any argument
    School() {}

    // destructor to free the memory allocated by the object when no longer needed , virtual indiacate it can be overriden by derived class
    virtual ~School() {}

    // virtual indicate it can be overriden by derived class, const indicate function doesn't modify the state of the object
    virtual void show() const
    {
        cout << "Name of the school: " << name << endl;
    }
};

class Programming : virtual public School
{
private:
    int courses;

public:
    Programming(const string &n, int c) :  courses(c) {} // Constructor
    void show() const
    {
        cout << "Number of programming courses: " << courses << endl;
    }
};

class Network : virtual public School
{
private:
    int courses;

public:
    Network(const string &n, int c) : School(n), courses(c) {} // Constructor
    void show() const
    {
        cout << "Number of network courses: " << courses << endl;
    }
};

// it will have no effect on inheritance behavior
class Student : virtual public Programming, virtual public Network
{
private:
    string name;
    int code;

public:
    Student(const string &s, const string &n, int c, int p, int net)
        : School(s), Programming(s, p), Network(s, net), name(n), code(c) {} // Constructor

    void show() const // Overriding the show function of School class
    {
        School::show();
        Programming::show();
        Network::show();
        cout << "Name of the student: " << name << endl;
        cout << "Student code: " << code << endl;
    }
};

int main()
{
    Student s("TBC", "Prabhat", 100, 10, 12);

    // reference to school base class is created from the object of student class
    School &p = s;

    // show function is called on the reference to the school base class
    p.show();
    
    return 0;
}