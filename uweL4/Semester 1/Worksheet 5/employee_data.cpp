// File name: employee_data.cpp
// Author: Aman Khadka
// Assignment: Worksheet 5 - Task 2 - Question 1
// Date: 01/11/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that takes input for employee name, tax number and salary and writes it to a binary file.

#include <iostream>
#include <string>
#include <fstream>
using namespace std;

// defines a constant value of 100 that can be used throughout the program in place of numeric value 100
#define NAME_LEN 100
#define NUM_EMPLOYEES 100

// structure that has three member
struct Employee
{
    char name[NAME_LEN]; // character array for employee name
    int taxNumber;
    int salary;
};

int main()
{
    // array of employee structure
    struct Employee emp[NUM_EMPLOYEES];

    // counter ( number of employee entered )
    int numEmployees = 0;

    // loop until user enters "fin"
    for (int i = 0; i < NUM_EMPLOYEES; i++)
    {
        cout << "Enter data for employee " << i + 1 << " or ( enter 'fin' to exit )" << endl;
        cout << "Name: ";
        cin.getline(emp[i].name, NAME_LEN);
        if (string(emp[i].name) == "fin")
        {
            cout << endl;
            cout << "Exited !!";
            break; // Terminate the loop
        }
        else
        {
            cout << "Tax number: ";
            cin >> emp[i].taxNumber;
            cin.ignore();

            cout << "Salary: ";
            cin >> emp[i].salary;
            cin.ignore();

            cout << endl;

            // increment number of employees
            numEmployees++;
        }
    }

    // Open file in binary append mode
    ofstream outfile("test.bin", ios::binary | ios::app);

    // Check if file was successfully opened
    if (!outfile.is_open())
    {
        cout << "Error opening file \n";
        return 0;
    }

    // Loop through each employee
    for (int i = 0; i < numEmployees; i++)
    {
        // Write employee data to file in binary format
        outfile.write(emp[i].name, NAME_LEN);
        // (c style casting)
        outfile.write((char *)&emp[i].taxNumber, sizeof(int)); // converts address of taxNumber to char pointer
        outfile.write((char *)&emp[i].salary, sizeof(int));    // converts address of salary to char pointer
    }

    // close file
    outfile.close();

    return 0;
}
