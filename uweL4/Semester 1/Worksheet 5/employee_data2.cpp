// File name: employee_data2.cpp
// Author: Aman Khadka
// Assignment: Worksheet 5 - Task 2 - Question 2
// Date: 01/11/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that reads employee data from binary file, filters the data based on salary threshold and write the filtered data to another binary file, it also calculates average salary.

#include <iostream>
#include <fstream>
using namespace std;

// defines a constant value of 100 that can be used throughout the program in place of numeric value 100
#define NAME_LEN 100
#define NUM_EMPLOYEES 100

// structure that has three member
struct Employee
{
    char name[NAME_LEN]; // character array for employee name
    int taxNumber;
    int salary;
};

int main()
{
    // array of employee structure
    struct Employee emp[NUM_EMPLOYEES];

    // Open file in binary read mode
    ifstream infile("test.bin", ios::binary);

    // Check if file was successfully opened
    if (!infile.is_open())
    {
        cout << "Error opening file \n";
        return 0;
    }

    cout << "-------------------------------------" << endl;
    cout << "DATA AVAILABLE IN TEST FILE" << endl;
    cout << "-------------------------------------" << endl;

    // Read and display employee data from file
    int numEmployees = 0;
    while (infile.read((char *)&emp[numEmployees], sizeof(Employee)))
    {
        cout << "Name: " << emp[numEmployees].name << endl;
        cout << "Tax Number: " << emp[numEmployees].taxNumber << endl;
        cout << "Salary: " << emp[numEmployees].salary << endl;
        cout << endl;

        // Increment number of employees
        numEmployees++;
    }

    // close file
    infile.close();

    // Read salary threshold from user
    int salaryThreshold;
    cout << "Enter salary threshold: ";
    cin >> salaryThreshold;
    cin.ignore();

    // counter ( number of employee entered )
    int totalSalary = 0;

    // Open output file in binary write mode
    ofstream outfile("data.bin", ios::binary);

    // Check if file was successfully opened
    if (!outfile.is_open())
    {
        cout << "Error opening file \n";
        return 0;
    }

    // Write employee data to output file
    for (int i = 0; i < numEmployees; i++)
    {
        // Check if salary is greater than threshold
        if (emp[i].salary > salaryThreshold)
        {
            // Update total salary
            totalSalary += emp[i].salary;

            outfile.write((char *)&emp[i], sizeof(Employee));
        }
    }

    // Calculate and print average salary
    if (numEmployees > 0)
    {
        double averageSalary = (double)totalSalary / numEmployees;
        cout << "Average salary: " << averageSalary << endl;
    }
    else
    {
        cout << "No employees found with salary greater than the threshold." << endl;
    }

    // close file
    outfile.close();

    // Open file in binary read mode
    ifstream infile2("data.bin", ios::binary);

    // Check if file was successfully opened
    if (!infile2.is_open())
    {
        cout << "Error opening file \n";
        return 0;
    }

    cout << endl;
    cout << "-------------------------------------" << endl;
    cout << "DATA AVAILABLE IN DATA FILE" << endl;
    cout << "-------------------------------------" << endl;

    // Read and display employee data from file
    while (infile2.read((char *)&emp[numEmployees], sizeof(Employee)))
    {
        cout << "Name: " << emp[numEmployees].name << endl;
        cout << "Tax Number: " << emp[numEmployees].taxNumber << endl;
        cout << "Salary: " << emp[numEmployees].salary << endl;
        cout << endl;

        // Increment number of employees
        numEmployees++;
    }

    // close file
    infile2.close();

    return 0;
}
