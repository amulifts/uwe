// File name: reverse_list.cpp
// Author: Aman Khadka
// Assignment: Worksheet 6 - Task 1 - Question 1 (a)
// Date: 01/11/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that takes input a list of integers from the user and uses the C++ STL library to reverse and display the original and reversed lists

#include <iostream>
#include <list>      // for list
#include <algorithm> // for algorithm

using namespace std;

class ReverseList
{
    // member variable to store the original and reversed List
    list<int> num;
    list<int> reversedNums;

public:
    // function declarations
    void takeInput();
    void reverseList();
    void displayList();
};

// function definition to take input from the user
void ReverseList::takeInput()
{
    cout << "Enter a list of integers seperated by space or ( enter 0 when finished ) : ";

    int n;
    while (cin >> n)
    {
        if (n == 0)
            break; // break out of the loop if the user enters 0
        num.push_back(n);
    }
}

// function definition to reverse the lists
void ReverseList::reverseList()
{
    // copy the elements of num to reversedNums
    reversedNums = num;

    // reverse the elements in reversedNums
    reverse(reversedNums.begin(), reversedNums.end());
}

// function definition to display original and reversed list
void ReverseList::displayList()
{
    cout << "Original List: ";
    for (int n : num)
        cout << n << " ";
    cout << endl;

    cout << "Reversed List: ";
    for (int n : reversedNums)
        cout << n << " ";
    cout << endl;
}

int main()
{
    // create a ReverseList object
    ReverseList rl;

    // get input from the user
    rl.takeInput();

    // reverse the list
    rl.reverseList();

    // display the list
    rl.displayList();
}
