// File name: hospital.cpp
// Author: Aman Khadka
// Assignment: Worksheet 6 - Task 3
// Date: January 11, 2023
// Copyright (C) 2023 Aman Khadka
//
// Description: This program simulates a hospital management system that keeps track of patients, their treatments and the departments of the hospital. It uses C++ STL container classes such as queue, vector, map and algorithm to perform various operations like patient tracking, token generation, department validation and generate report. It defines classes and structs to represent a Patient, a Department, and the Hospital.

#include <queue>     // for queue
#include <vector>    // for vector
#include <algorithm> // for count
#include <string>
#include <iostream>
#include <map> // for map

using namespace std;

// Struct to represent a patient
struct Patient
{
    string patientid;
    int social_id;
    string occupation;
    string disease;
    string treatment;
    bool previousTreated;
    int tokenNumber;
    string dateofvisit;
};

// Class to represent a department
class Department
{
public:
    // queue to store patients waiting for treatment
    queue<Patient> patientQueue;

    // method to add a patient to the department
    void AddPatient(Patient patient)
    {
        // add the patient to the end of the queue
        patientQueue.push(patient);
    }

    // method to treat the next patient in the department
    Patient TreatNextPatient()
    {
        // get the patient at the front of the queue
        Patient nextPatient = patientQueue.front();

        // remove the patient from the queue
        patientQueue.pop();

        return nextPatient;
    }

    // method to get the number of patients waiting in the department
    int NumWaitingPatients()
    {
        return patientQueue.size();
    }
};

// Class to represent the hospital
class Hospital
{
public:
    // mapping of departments and their corresponding department class
    map<string, Department> departments;
    int nextTokenNumber;
    int numTreatedPatient;
    int numPatientToday;

    // constructor to initialize variables
    Hospital() : nextTokenNumber(1), numTreatedPatient(0), numPatientToday(0) {}

    // method to add a patient to a department
    void AddPatient(string departmentName, Patient patient)
    {
        // List of valid departments
        vector<string> validDepartments = {"Dermatology", "Cardiology", "Gynecology", "Pathology"};

        // convert the departmentName to lowercase
        for (auto &x : validDepartments)
        {
            transform(x.begin(), x.end(), x.begin(), ::tolower);
        }

        // Check if the departmentName is not in the validDepartments vector, then throw an exception
        if (count(validDepartments.begin(), validDepartments.end(), departmentName) == 0)
        {
            throw invalid_argument("Invalid department name.");
        }

        // assign the next token number to the patient
        patient.tokenNumber = nextTokenNumber;
        nextTokenNumber++;                               // increment the next token number
        numPatientToday++;                               // increment the number of patients seen today
        departments[departmentName].AddPatient(patient); // add the patient to the specified department

        cout << "----------------------------------" << endl;
        cout << "Patient added to department " << departmentName << " with token number " << patient.tokenNumber << endl;
        cout << "----------------------------------" << endl;
    }

    // method to treat the next patient in a department
    Patient TreatNextPatient(string departmentName)
    {
        // call the TreatNextPatient method of the specified department
        Patient patient = departments[departmentName].TreatNextPatient();

        // increment the number of patients treated
        numTreatedPatient++;
        return patient;
    }

    // method to get the number of patients waiting in a specific department
    int NumWaitingPatients(string departmentName)
    {
        // find the specified department in the map
        auto it = departments.find(departmentName);

        // check if the department is not found in the map, then return 0
        if (it == departments.end())
        {
            return 0;
        }

        // return the number of patients waiting in the specified department
        return it->second.NumWaitingPatients();
    }

    // method to get the total number of patients waiting in all departments
    int NumWaitingPatients()
    {
        int totalWaiting = 0;
        for (auto department : departments) // iterate through the map of departments
        {
            // add the number of patients waiting in each department
            totalWaiting += department.second.NumWaitingPatients();
        }
        return totalWaiting;
    }

    int NumTreatedPatients()
    {
        return numTreatedPatient;
    }

    int NumPatientToday()
    {
        return numPatientToday;
    }

    // method to generate a report for a specific patient
    void generatePatientReport(string patientId)
    {
        cout << "------------------------" << endl;
        for (auto department : departments) // iterate through the map of departments
        {
            queue<Patient> patientQueue = department.second.patientQueue;
            while (!patientQueue.empty()) // iterate through the queue of patients
            {
                Patient patient = patientQueue.front();
                if (patient.patientid == patientId) // check if the patient's ID matches the specified ID
                {
                    // output the patient's information
                    cout << "Patient ID: " << patient.patientid << endl;
                    cout << "Social ID: " << patient.social_id << endl;
                    cout << "Occupation: " << patient.occupation << endl;
                    cout << "Disease: " << patient.disease << endl;
                    cout << "Treatment: " << patient.treatment << endl;
                    cout << "Previously treated: " << patient.previousTreated << endl;
                    cout << "Token Number: " << patient.tokenNumber << endl;
                    cout << "Date of Visit: " << patient.dateofvisit << endl;
                    cout << "------------------------" << endl;
                    return;
                }
                patientQueue.pop(); // remove the patient from the queue
            }
        }
        cout << "Patient not found." << endl;
        cout << "------------------------" << endl;
    }
};

// driver code
int main()
{
    Hospital hospital;

    while (true)
    {
        cout << "Menu:" << endl;
        cout << "1. Add patient" << endl;
        cout << "2. Treat next patient" << endl;
        cout << "3. Get number of waiting patients for a department" << endl;
        cout << "4. Get total number of waiting patients" << endl;
        cout << "5. Get total number of treated patients" << endl;
        cout << "6. Get total number of patients today" << endl;
        cout << "7. Generate patient report" << endl;
        cout << "8. Quit" << endl;

        int choice;
        cout << "Enter your choice: ";
        cin >> choice;

        if (choice == 1)
        {
            string departmentName;
            cout << "Enter department name: ";
            cin >> departmentName;

            Patient patient;
            cout << "Enter patient ID: ";
            cin >> patient.patientid;
            cout << "Enter social ID: ";
            cin >> patient.social_id;
            cout << "Enter occupation: ";
            cin >> patient.occupation;
            cout << "Enter disease: ";
            cin >> patient.disease;
            cout << "Enter treatment: ";
            cin >> patient.treatment;
            cout << "Enter previous treated (0 or 1): ";
            cin >> patient.previousTreated;
            cout << "Enter date of visit: ";
            cin >> patient.dateofvisit;

            try
            {
                hospital.AddPatient(departmentName, patient);
            }
            catch (invalid_argument &e)
            {
                cout << e.what() << endl;
            }
        }
        else if (choice == 2)
        {
            string departmentName;
            cout << "Enter department name: ";
            cin >> departmentName;

            if (hospital.NumWaitingPatients(departmentName) == 0)
            {
                cout << "No patients waiting in " << departmentName << endl;
            }
            else
            {
                Patient patient = hospital.TreatNextPatient(departmentName);
                cout << "Patient treated: " << patient.patientid << endl;
            }
        }
        else if (choice == 3)
        {
            string departmentName;
            cout << "Enter department name: ";
            cin >> departmentName;

            int numWaiting = hospital.NumWaitingPatients(departmentName);
            cout << "Number of waiting patients in " << departmentName << ": " << numWaiting << endl;
        }
        else if (choice == 4)
        {
            int totalWaiting = hospital.NumWaitingPatients();
            cout << "Total number of waiting patients: " << totalWaiting << endl;
        }
        else if (choice == 5)
        {
            int totalTreated = hospital.NumTreatedPatients();
            cout << "Total number of treated patients: " << totalTreated << endl;
        }
        else if (choice == 6)
        {
            int totalToday = hospital.NumPatientToday();
            cout << "Total number of patients today: " << totalToday << endl;
        }
        else if (choice == 7)
        {
            string patientId;
            cout << "Enter patient ID: ";
            cin >> patientId;

            hospital.generatePatientReport(patientId);
        }
        else if (choice == 8)
        {
            break;
        }
    }

    return 0;
}