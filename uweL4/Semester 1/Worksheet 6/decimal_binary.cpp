// File name: decimal_binary.cpp
// Author: Aman Khadka
// Assignment: Worksheet 6 - Task 1 - Question 1 (b)
// Date: 01/11/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that takes in a decimal number and converts it to its binary equivalent using bitset and stack data structures. Program prompts user to enter decimal numbers and continues until user enters "exit" to exit the program.

#include <iostream>
#include <string>
#include <stack>  // for stack
#include <bitset> // for bitset

using namespace std;

class DecimalToBinary
{
    // member variable to store the decimal number
    int decimal;

    // stack to store the binary digits
    stack<int> binary;

public:
    DecimalToBinary(int decimal)
    {
        this->decimal = decimal;
    }
    // function prototypes
    void convert();
    void display();
};

// implementation of the convert function
void DecimalToBinary::convert()
{
    /*  // divide the decimal number by 2 repeatedly
     // and store the remainder in the stack
     while (decimal > 0)
     {
         binary.push(decimal % 2);
         decimal /= 2;
     } */

    // creating bitset object for decimal number
    bitset<8> b(decimal);

    // store the binary digits in the stack
    for (auto i = 0; i < b.size(); i++)
    {
        binary.push(b[i]);
    }
}

// implementation of the display function
void DecimalToBinary::display()
{
    cout << "Binary: ";

    // pop the elements from the stack and print them
    while (!binary.empty())
    {
        cout << binary.top();
        binary.pop();
    }

    cout << endl;
}

int main()
{
    string input;

    cout << "Enter a decimal number ('exit' to exit): ";
    cin >> input;

    while (input != "exit")
    {
        int decimal = stoi(input);
        // create a DecimalToBinary object for the given decimal number
        DecimalToBinary dtb(decimal);

        // convert the decimal number to binary
        dtb.convert();

        // display the binary number
        dtb.display();

        cout << "Enter a decimal number ('exit' to exit): ";
        cin >> input;
    }

    if (input == "exit")
    {
        cout << "Exited!!";
    }

    return 0;
}