// File name: queue_operations.cpp
// Author: Aman Khadka
// Assignment: Worksheet 6 - Task 1 - Question 1 (c)
// Date: 01/11/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that demonstrates various queue operations using the C++ STL library. It includes enqueuing and dequeuing elements, printing the front, back, and size of the queue, checking if the queue is empty, and sorting and reversing the elements in the queue. The program uses the queue, vector, and algorithm headers from the STL.

#include <iostream>
#include <queue>     // for queue
#include <algorithm> // for sort and reverse
#include <vector>    // for vector

using namespace std;

// Class to perform queue operations
class QueueOperations
{
    // Queue to store the elements
    queue<int> q;

    // Vector to store the elements
    vector<int> v;

public:
    void enqueue(int x) { q.push(x); }

    void dequeue() { q.pop(); }

    void printFront() { cout << "Front element: " << q.front() << endl; }

    void printBack() { cout << "Back element: " << q.back() << endl; }

    void printSize() { cout << "Queue size: " << q.size() << endl; }

    void printAllElements();

    void sortElements();

    void reverseElements();

    void isEmpty();
};

void QueueOperations::printAllElements()
{
    cout << "All elements in the queue before any operations: ";
    while (!q.empty())
    {
        // Add the front element to the vector
        v.push_back(q.front());
        // Remove the front element
        q.pop();
    }
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

void QueueOperations::sortElements()
{
    // Sort the vector
    sort(v.begin(), v.end());

    // Add the elements to the queue
    for (int i = 0; i < v.size(); i++)
    {
        q.push(v[i]);
    }

    // Print the elements in the queue
    cout << "Sorted elements in the queue: ";
    while (!q.empty())
    {
        // Print the front element
        cout << q.front() << " ";
        // Remove the front element
        q.pop();
    }
    cout << endl;
}

void QueueOperations::reverseElements()
{
    // Reverse the vector
    reverse(v.begin(), v.end());

    // Add the elements to the queue
    for (int i = 0; i < v.size(); i++)
    {
        q.push(v[i]);
    }

    // Print the elements in the queue
    cout << "Reversed elements in the queue: ";
    while (!q.empty())
    {
        // Print the front element
        cout << q.front() << " ";
        // Remove the front element
        q.pop();
    }
    cout << endl;
}

void QueueOperations::isEmpty()
{
    if (q.empty())
    {
        cout << "Queue is empty" << endl;
    }
    else
    {
        cout << "Queue is not empty" << endl;
    }
}

int main()
{
    QueueOperations q;

    cout << "----------------------------------------" << endl;
    cout << "Queue operations using STL containers:" << endl;
    cout << "----------------------------------------" << endl;

    // Add elements to the queue
    q.enqueue(10);
    q.enqueue(40);
    q.enqueue(20);
    q.enqueue(30);
    q.enqueue(50);

    // Perform queue operations
    q.dequeue();          // Remove the front element
    q.printFront();       // Print the front element
    q.printBack();        // Print the back element
    q.printSize();        // Print the size of the queue
    q.isEmpty();          // Check if the queue is empty
    q.printAllElements(); // Print all the elements in the queue

    cout << "----------------------------------------" << endl;
    cout << "Queue operations using STL algorithms:" << endl;
    cout << "----------------------------------------" << endl;
    q.sortElements();    // Sort the elements in the queue
    q.reverseElements(); // Reverse the elements in the queue
    return 0;
}
