#include <iostream>
using namespace std;

int main()
{
    char alphabet[26];
    int i;

    // Fill the alphabet array with the letters A-Z
    for (i = 0; i < 26; ++i)
    {
        alphabet[i] = 'A' + i;
    }

    // Output the alphabet array
    for (i = 0; i < 26; ++i)
    {
        cout << alphabet[i] << endl;
    }

    return 0;
}
