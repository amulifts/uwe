#include <iostream>
using namespace std;

char *fillArray()
{
    static char characters[6] = {'H', 'e', 'l', 'l', 'o', '\0'};
    return characters;
}

int main()
{
    char *values = fillArray();
    for (int i = 0; i < 6; i++)
    {
        cout << values[i];
    }
    return 0;
}
