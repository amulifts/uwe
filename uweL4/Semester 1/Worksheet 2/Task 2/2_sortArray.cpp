#include <iostream>
using namespace std;

void SortArray(int values[]);

int main()
{
  int values[75];
  // we can add code that add values to the array here
  SortArray(values);
  return 0;
}

void SortArray(int values[])
{
  for (int i = 0; i < 75; ++i)
  {
    if (values[i] < values[i - 1])
    {
      values[i] = values[i - 1];
    }
  }
}
