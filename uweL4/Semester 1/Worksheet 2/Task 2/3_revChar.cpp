#include <iostream>
#include <cstring> // for strlen()

using namespace std;

int main()
{
    char saying[50], revSaying[50];
    int i;

    cout << "\nEnter a saying: ";
    cin.getline(saying, sizeof(saying) / sizeof(char));

    for (i = 0; i < strlen(saying); i++)
    {
        revSaying[i] = saying[strlen(saying) - 1 - i];
    }

    cout << "Reversed saying: " << revSaying << endl;

    return 0;
}
