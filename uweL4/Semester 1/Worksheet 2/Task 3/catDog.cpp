// File name: catDog.cpp
// Author: Aman Khadka
// Assignment: Worksheet 2 - Task 3 - Question 2
// Date: January 08, 2023
// Copyright (C) 2023 Aman Khadka
//
// Description: This program demonstrates the use of overloaded functions to display the names and how the animal speaks of  dog and cat objects

#include <iostream>
#include <string>
using namespace std;

struct Dog
{
    string dogName;
} dog;

struct Cat
{
    string catName;
} cat;

// Function prototypes for overloaded functions
void speak(Dog dog);
void speak(Cat cat);

// Main function
int main()
{
    // Assign names to the dog and cat
    dog.dogName = "Spot";
    cat.catName = "Tiger";

    // Call the overloaded functions to display the names and how they speak
    speak(dog);
    speak(cat);

    return 0;
}

// Function definitions for overloaded functions
void speak(Dog dog)
{
    cout << dog.dogName << " says woof" << endl;
}

// Function definitions for overloaded functions
void speak(Cat cat)
{
    cout << cat.catName << " says meow" << endl;
}