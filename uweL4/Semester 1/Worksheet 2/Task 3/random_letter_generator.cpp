// File name: random_letter_generator.cpp
// Author: Aman Khadka
// Assignment: Worksheet 2 - Task 3 - Question 1
// Date: January 08, 2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that generates 15 random uppercase letters, using a seed value input by the user and the C++ standard library's random number generator

#include <iostream>
#include <random>

using namespace std;

// Function prototype
char GenerateALetter();

int main()
{
    cout << "Enter a seed value: ";
    int seed;
    cin >> seed;

    // Seed the random number generator
    srand(seed);

    // Generate 15 random letters
    int i = 0;
    do
    {
        cout << GenerateALetter() << ", ";
        ++i;
    } while (i < 15);

    return 0;
}

char GenerateALetter()
{
    char letter;

    // Generate a random number between 0 to 25 and add 65 resulting in random letters from A to Z
    letter = rand() % 26 + 65;

    return letter;
}