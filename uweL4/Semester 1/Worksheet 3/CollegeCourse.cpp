// File name: CollegeCourse.cpp
// Author: Aman Khadka
// Assignment: Worksheet 3 - Task 2 - Question 1
// Date: 01/09/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: This program demonstrates the use of classes and objects in C++ by creating CollegeCourse, Instructor and Classroom classes and displaying their information.

#include <iostream>
#include <string>
using namespace std;

class Instructor
{
    string firstName;
    string lastName;
    int officeNumber;

public:
    // Constructor that takes first name, last name and office number as arguments
    Instructor(string firstname, string lastname, int officenumber) : firstName(firstname), lastName(lastname), officeNumber(officenumber) {}

    void display(); // function prototype / declaration
};

void Instructor::display()
{
    cout << "Instructor Name: " << firstName << " " << lastName << endl;
    cout << "Office Number: " << officeNumber << endl;
}

class Classroom
{
    string building;
    int roomNumber;

public:
    // Constructor that takes building and room number as arguments
    Classroom(string building, int roomNumber) : building(building), roomNumber(roomNumber) {}

    void display(); // function prototype / declaration
};

void Classroom::display()
{
    cout << "Classroom: " << building << " " << roomNumber << endl;
}

class CollegeCourse
{
    // Instructor and Classroom objects are created as private members of CollegeCourse class
    Instructor instructor;
    Classroom classroom;
    int credits;

public:
    // Constructor that takes first name, last name, office number, building, room number and credits as arguments
    CollegeCourse(string firstName, string lastName, int officeNumber, string building, int roomNumber, int credits) : instructor(firstName, lastName, officeNumber), classroom(building, roomNumber), credits(credits) {}

    void display(); // function prototype / declaration
};

void CollegeCourse::display()
{
    instructor.display();
    classroom.display();
    cout << "Credits: " << credits << endl;
}

int main()
{
    // Creating two CollegeCourse objects and displaying their values
    CollegeCourse c1("Aman", "Khadka", 110, "TBC Thapathali", 1, 100);
    CollegeCourse c2("Khadka", "Ji", 220, "UWE London", 2, 200);
    c1.display();
    cout << "\n";
    c2.display();
    return 0;
}