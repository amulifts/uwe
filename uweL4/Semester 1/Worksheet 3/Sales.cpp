// File name: Sales.cpp
// Author: Aman Khadka
// Assignment: Worksheet 3 - Task 2 - Question 2
// Date: 01/09/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: This program demonstrates the use of friend functions, classes and arrays in C++ by creating an object of the class Sale and Salesperson, and using a friend function to display the data members of the class Sale and Salesperson.

#include <iostream>
#include <string>
using namespace std;

// Forward declaration of the class Salesperson
class Salesperson;

// Sale class to hold data for a sales transaction
class Sale
{
    int dayOfMonth;
    double amountOfSale;
    int salespersonID;

public:
    // Constructor to initialize the data members of the class Sale
    Sale(int dayOfMonth, double amountOfSale, int salespersonID) : dayOfMonth(dayOfMonth), amountOfSale(amountOfSale), salespersonID(salespersonID) {}

    // Friend function declaration to display the data members of the class Sale
    friend void display(Sale sale, Salesperson salesperson);
};

// Salesperson class to hold data for a salesperson
class Salesperson
{
    int salespersonID;
    string lastName;

public:
    // Constructor to initialize the data members of the class Salesperson
    Salesperson(int salespersonID, string lastName) : salespersonID(salespersonID), lastName(lastName) {}

    // Friend function declaration to display the data members of the class Salesperson
    friend void display(Sale sale, Salesperson salesperson);
};

// Friend function definition to display the data members of the class Sale and Salesperson
void display(Sale sale, Salesperson salesperson)
{
    cout << "Day of the month: " << sale.dayOfMonth << endl;
    cout << "Amount of the sale: " << sale.amountOfSale << endl;
    cout << "Salesperson ID: " << salesperson.salespersonID << endl;
    cout << "Salesperson Last Name: " << salesperson.lastName << endl;
}

int main()
{
    // Create an object of the class Sale
    Sale sale(2, 200, 1);
    // Create an object of the class Salesperson
    Salesperson salesperson(1, "Khadka");
    // Call the friend function display to display the data members of the class Sale and Salesperson
    display(sale, salesperson);
    return 0;
}
