// File name: Sales2.cpp
// Author: Aman Khadka
// Assignment: Worksheet 3 - Task 2 - Question 3
// Date: 01/09/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: This program demonstrates the use of friend functions, classes and arrays in C++ by prompting user for sales transaction data, validate and display all the Sale and Salesperson data in a loop until the sentinel value is entered.

#include <iostream>
#include <string>
using namespace std;

// Forward declaration of the class Salesperson
class Salesperson;

// Sale class to hold data for a sales transaction
class Sale
{
    int dayOfMonth;
    double amountOfSale;
    int salespersonID;

public:
    // Constructor to initialize the data members of the class Sale
    Sale(int dayOfMonth, double amountOfSale, int salespersonID) : dayOfMonth(dayOfMonth), amountOfSale(amountOfSale), salespersonID(salespersonID) {}

    // Friend function declaration to display the data members of the class Sale
    friend void display(Sale sale, Salesperson salesperson);

    // Function to return the private salesperson ID number
    int getSalespersonID() { return salespersonID; }
};

// Salesperson class to hold data for a salesperson
class Salesperson
{
    int salespersonID;
    string lastName;

public:
    // Constructor to initialize the data members of the class Salesperson
    Salesperson(int salespersonID, string lastName) : salespersonID(salespersonID), lastName(lastName) {}

    // Friend function declaration to display the data members of the class Salesperson
    friend void display(Sale sale, Salesperson salesperson);

    // Function to return the private salesperson ID number
    int getSalespersonID() { return salespersonID; }
};

// Friend function definition to display the data members of the class Sale and Salesperson
void display(Sale sale, Salesperson salesperson)
{
    cout << "Day of the month: " << sale.dayOfMonth << endl;
    cout << "Amount of the sale: " << sale.amountOfSale << endl;
    cout << "Salesperson ID: " << salesperson.salespersonID << endl;
    cout << "Salesperson Last Name: " << salesperson.lastName << endl;
}

int main()
{
    // Array of 5 Salesperson objects
    Salesperson salespersons[5] = {
        Salesperson(1, "Khadka"),
        Salesperson(2, "Sahu"),
        Salesperson(3, "Bhattarai"),
        Salesperson(4, "Singh"),
        Salesperson(5, "Adhikari")};

    // Sentinel value for the Sales transaction data
    const int SENTINEL = -1;

    // Loop until the sentinel value is entered
    while (true)
    {
        cout << "Enter the day of the month (-1 to exit): ";
        int dayOfMonth;
        cin >> dayOfMonth;

        // Check if the sentinel value is entered
        if (dayOfMonth == SENTINEL)
        {
            break;
        }

        cout << "Enter the amount of the sale: ";
        double amountOfSale;
        cin >> amountOfSale;

        cout << "Enter the salesperson ID: ";
        int salespersonID;
        cin >> salespersonID;

        // Check if the salesperson ID is valid or not
        if (salespersonID < 1 || salespersonID > 5)
        {
            cout << "Invalid salesperson ID." << endl;
            continue;
        }
        // Create an object of the class Sale
        Sale sale(dayOfMonth, amountOfSale, salespersonID);
        // Get the salesperson object from the array
        Salesperson salesperson = salespersons[salespersonID - 1];
        // Call the friend function display to display the data members of the class Sale and Salesperson
        display(sale, salesperson);
    }

    return 0;
}
