// File name: transcript.cpp
// Author: Aman Khadka
// Assignment: Worksheet 1 - Task 3 - Question 3
// Date: 01/07/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that demonstrates using a class and its methods for calculating a student's GPA. The program uses a constructor to initialize the GPA to 0, a readValues function to get the GPA from the user, setter and getter functions to set and return the GPA, and a displayMessage function that displays a message based on the GPA entered.

#include <iostream>
using namespace std;

class Transcript
{
    double gpa;

public:
    Transcript();          // constructor declaration
    void readValues();     // read values function declaration
    void setGpa(double);   // setter function declaration
    double getGpa();       // getter function declaration
    void displayMessage(); // getter function declaration
};

// Constructor to initialize gpa to
Transcript::Transcript()
{
    gpa = 0;
}

// read values function to read gpa from user
void Transcript::readValues()
{
    cout << "Enter your GPA: ";
    cin >> gpa;
    setGpa(gpa);
}

// Setter function to set gpa
void Transcript::setGpa(double gpa)
{
    this->gpa = gpa;
}

// Getter function to return gpa
double Transcript::getGpa()
{
    return gpa;
}

// Getter function to return message based on gpa
void Transcript::displayMessage()
{
    if (gpa >= 0.0 && gpa <= 4.0)
    {
        if (gpa >= 0.0 && gpa <= 0.99)
            cout << "Failed semester—registration suspended" << endl;
        else if (gpa >= 1.0 && gpa <= 1.99)
            cout << "On probation for next semester" << endl;
        else if (gpa >= 2.0 && gpa <= 2.99)
            cout << "(no message)" << endl;
        else if (gpa >= 3.0 && gpa <= 3.49)
            cout << "Dean’s list for semester" << endl;
        else if (gpa >= 3.5 && gpa <= 4.0)
            cout << "Highest honors for semester" << endl;
    }
    else
    {
        cout << "Invalid GPA" << endl;
    }
}

// driver program
int main()
{
    // create an object of class Transcript
    Transcript t;

    // get gpa from user alos set gpa
    t.readValues();

    // display message
    t.displayMessage();

    return 0;
}
