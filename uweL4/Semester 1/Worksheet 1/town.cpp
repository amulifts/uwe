// File name: town.cpp
// Author: Aman Khadka
// Assignment: Worksheet 1 - Task 3 - Question 4
// Date: 01/07/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that demonstrates using a class, constructor, setter and getter methods, and a loop to simulate the growth of a town's population. The program uses a Town class that has member variables for the population and annual increase, as well as member functions to set and get these values, and a function to increase the population based on the annual increase. The main function creates a Town object with starting population and annual increase values, then simulates population growth and calculates the number of years it takes for the population to pass 20,000.

#include <iostream>
using namespace std;

class Town
{
    int population;
    int annualIncrease;

public:
    // constructor for Town class with default values for population and annual increase
    Town(int startingPop, int annualIncrease) : population(startingPop), annualIncrease(annualIncrease) {}

    // getter to return population
    int getPop() { return population; }

    // setter to set population
    void setPop(int pop) { population = pop; }

    // getter to return annual increase
    int getAnnualIncrease() { return annualIncrease; }

    // setter to set annual increase
    void setAnnualIncrease(int annualIncrease) { this->annualIncrease = annualIncrease; }

    // increase population by annual increase
    void increasePop()
    {
        population += population * (annualIncrease / 100.0);
    }
};

// main function
int main()
{
    // create a Town object with starting population 900 and annual increase 10%
    Town t(900, 10);

    // variable to store number of years
    int countYears;

    // loop until population is greater than 20,000
    // on each iteration, increase population by annual increase and print current year and population
    for (countYears = 0; t.getPop() < 20000; countYears++)
    {
        t.increasePop();
        cout << "Year " << countYears << ": " << t.getPop() << endl;
    }

    // display number of years it took for population to pass 20,000
    cout << "It will take " << countYears << " years for the population to pass 20,000." << endl;
    return 0;
}
