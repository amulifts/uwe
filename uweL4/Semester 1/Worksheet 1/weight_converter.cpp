// File name: weight_converter.cpp
// Author: Aman Khadka
// Assignment: Worksheet 1 - Task 3 - Question 1
// Date: 01/07/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that demonstrates using a class and its methods to convert weight from pounds to kilograms. The program uses a WeightConverter class that has a constructor, a setter and getter for a float variable weightInPounds and a getter function for performing the conversion from pounds to kilograms. The class also has a display function that displays the weight in kilograms. The main function creates a WeightConverter object, prompts the user to enter a value for weight in pounds, uses the setter function to set the value, calls the getter function to perform the conversion and display function to display the result.

#include <iostream>
using namespace std;

class WeightConverter
{
    float weightInPounds;

public:
    WeightConverter();            // constructor declaration
    void setWeightInPounds();     // setter function declaration
    float getWeightInPounds();    // getter function declaration
    float getWeightInKilograms(); // getter function declaration
    void display();               // display function declaration
};

// Constructor to initialize weightInPounds to 0
WeightConverter::WeightConverter()
{
    weightInPounds = 0;
}

// Setter function to set weightInPounds to user input
void WeightConverter::setWeightInPounds()
{
    cout << "Enter weight in pounds: ";
    cin >> weightInPounds;
}

// Getter function to return weightInPounds
float WeightConverter::getWeightInPounds()
{
    return weightInPounds;
}

// Getter function to return weightInPounds converted to kilograms
float WeightConverter::getWeightInKilograms()
{
    return weightInPounds * 0.453592;
}

// display function to display weight in kilograms
void WeightConverter::display()
{
    cout << "Weight in Kg is: " << getWeightInKilograms();
}

// driver program
int main()
{
    // create an object of class WeightConverter
    WeightConverter wC;

    // set weight in pounds
    wC.setWeightInPounds();

    // display weight in Kilograms
    wC.display();

    return 0;
}