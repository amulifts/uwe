// File name: calculator.cpp
// Author: Aman Khadka
// Assignment: Worksheet 1 - Task 3 - Question 2
// Date: 01/07/2023
// Copyright (C) 2023 Aman Khadka
//
// Description: A program that demonstrates using a class and its methods to perform various mathematical operations. The program uses a Calculator class that has a constructor, setters and getters for two integer variables m and n, and getter functions for performing basic mathematical operations such as addition, subtraction, multiplication, division and remainder operations using m and n as operands . The main function creates a Calculator object, prompts the user to enter values for m and n, uses the setter functions to set the values and the getter functions to perform the mathematical operations and display the results.

#include <iostream>
using namespace std;

class Calculator
{
    int m, n;

public:
    Calculator();          // constructor declaration
    void readValues();     // read values function declaration
    void setM(int);        // setter function declaration
    void setN(int);        // setter function declaration
    int getM();            // getter function declaration
    int getN();            // getter function declaration
    int getSum();          // getter function declaration
    int getDifference1();  // getter function declaration
    int getDifference2();  // getter function declaration
    int getProduct();      // getter function declaration
    double getQuotient1(); // getter function declaration
    double getQuotient2(); // getter function declaration
    int getRemainder1();   // getter function declaration
    int getRemainder2();   // getter function declaration
};

// Constructor to initialize m and n to 0
Calculator::Calculator()
{
    m = 0;
    n = 0;
}

// read values function to read m and n from user
void Calculator::readValues()
{
    cout << "Enter value for m: ";
    cin >> m;
    cout << "Enter value for n: ";
    cin >> n;
    setM(m);
    setN(n);
}

// Setter function to set m
void Calculator::setM(int m)
{
    this->m = m;
}

// Setter function to set n
void Calculator::setN(int n)
{
    this->n = n;
}

// Getter function to return m
int Calculator::getM()
{
    return m;
}

// Getter function to return n
int Calculator::getN()
{
    return n;
}

// Getter function to return sum of m and n
int Calculator::getSum()
{
    return m + n;
}

// Getter function to return difference of m and n
int Calculator::getDifference1()
{
    return m - n;
}

// Getter function to return difference of n and m
int Calculator::getDifference2()
{
    return n - m;
}

// Getter function to return product of m and n
int Calculator::getProduct()
{
    return m * n;
}

// Getter function to return quotient of m and n
double Calculator::getQuotient1()
{
    return (double)m / n;
}

// Getter function to return quotient of n and m
double Calculator::getQuotient2()
{
    return (double)n / m;
}

// Getter function to return remainder of m and n
int Calculator::getRemainder1()
{
    return m % n;
}

// Getter function to return remainder of n and m
int Calculator::getRemainder2()
{
    return n % m;
}

// driver program
int main()
{
    // create an object of class Calculator
    Calculator c;

    // get m and n from user also set m and n
    c.readValues();

    // display sum
    cout << c.getM() << " + " << c.getN() << " = " << c.getSum() << endl;

    // display difference
    cout << c.getM() << " - " << c.getN() << " = " << c.getDifference1() << endl;
    cout << c.getN() << " - " << c.getM() << " = " << c.getDifference2() << endl;

    // display product
    cout << c.getM() << " * " << c.getN() << " = " << c.getProduct() << endl;

    // display quotient
    cout << c.getM() << " / " << c.getN() << " = " << c.getQuotient1() << endl;
    cout << c.getN() << " / " << c.getM() << " = " << c.getQuotient2() << endl;

    // display remainder
    cout << c.getM() << " % " << c.getN() << " = " << c.getRemainder1() << endl;
    cout << c.getN() << " % " << c.getM() << " = " << c.getRemainder2() << endl;

    return 0;
}
